import java.util.GregorianCalendar;


@SuppressWarnings("serial")
public class MyCalendar extends GregorianCalendar {
    public MyCalendar(int day, int month, int year) {
        super(year, month, day);
    }
    
    @Override
    public String toString() {
        return get(GregorianCalendar.DAY_OF_WEEK) + "/" + get(GregorianCalendar.MONTH) + "/"
                + get(GregorianCalendar.YEAR);
    }
}
